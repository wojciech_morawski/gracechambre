// ========================================
// GULP CONFIG
// http://webdesign.tutsplus.com/tutorials/the-command-line-for-web-design-live-reload-browsersync--cms-23455
// https://github.com/sogko/gulp-recipes
// https://github.com/gulpjs/gulp
// http://blog.nodejitsu.com/npmawesome-9-gulp-plugins/
// ========================================
// ========================================
//   REQUIRE
// ========================================
'use strict';

var autoprefixer = require('gulp-autoprefixer');
var browserSync  = require('browser-sync');
var changed      = require('gulp-changed');
var cmq          = require('gulp-combine-media-queries');
var concat       = require('gulp-concat');
var del          = require('del');
var gulp         = require('gulp');
var gutil        = require('gulp-util');
var imagemin     = require('gulp-imagemin');
var jade         = require('gulp-jade');
var jshint       = require('gulp-jshint');
var less         = require('gulp-less');
var minifyCSS    = require('gulp-minify-css');
var nodemon      = require('gulp-nodemon');
var rename       = require('gulp-rename');
var size         = require('gulp-size');
var uglify       = require('gulp-uglify');

var paths = {
  source : {
    styles  : './source/less/code73-style.less',
    jade    : ['./source/jade/**/*.jade',
                  '!./source/**/config.jade',
                  '!./source/**/docwrapper.jade'],
    js      : './source/js/',
    images  : './source/images/',
    vendors : [ 'node_modules/jquery/dist/jquery.js',
                'node_modules/modernizr/modernizr.js',
                'node_modules/velocity-animate/velocity.js',
                'node_modules/velocity-animate/velocity.ui.js'
              ],
  },
  build : {
    html    : './build/',
    css     : './build/css/',
    images  : './build/images/',
    js      : './build/js/',
    jspre   : './build/jspre'
  }
};

// ========================================
//   CORE TASKS
// ========================================
// ––––––––––––––––––––––––––––––––– CLEAN
gulp.task('clean', function() {
  return del.sync(['./build/**', '!./build', './source/js/vendors.js']);
});

// ––––––––––––––––––––––––––––––––– HTML
gulp.task('html', function() {
  return gulp.src(paths.source.jade)
    .pipe(size())
    .pipe(jade())
    .pipe(size())
    .pipe(gulp.dest(paths.build.html))
    .pipe(browserSync.reload({stream:true}))
});

// ––––––––––––––––––––––––––––––––– CSS
gulp.task('css', function () {
  return gulp.src(paths.source.styles)
    .pipe(changed('./build/css'))
    .pipe(size())
    .pipe(less())
    .pipe(cmq({log: true}))
    .pipe(autoprefixer())
    .pipe(minifyCSS())
    .pipe(rename('style.css'))
    .pipe(size())
    .pipe(gulp.dest(paths.build.css))
    .pipe(browserSync.reload({stream:true}))
});

// ––––––––––––––––––––––––––––––––– JS-VENDOR
gulp.task('vendor', function() {
  return   gulp.src(paths.source.vendors)
    .pipe(size())
    .pipe(concat('0-vendors.js'))
    .pipe(size())
    .pipe(gulp.dest(paths.build.jspre))
 });

// ––––––––––––––––––––––––––––––––– JS-CUSTOM
gulp.task('js-custom', ['vendor'], function () {
  return gulp.src(paths.source.js + '/**/*.js')
    .pipe(size())
    .pipe(concat('1-custom.js'))
    .pipe(size())
    .pipe(gulp.dest(paths.build.jspre))
});

// ––––––––––––––––––––––––––––––––– JS
gulp.task('js', ['js-custom'], function () {
  return gulp.src(paths.build.jspre + '/**/*.js')
    .pipe(size())
    .pipe(concat('single.js'))
    .pipe(uglify())
    .pipe(size())
    .pipe(gulp.dest(paths.build.js))
});

// ––––––––––––––––––––––––––––––––– IMAGES-PNG
gulp.task('images-png', function() {
  return gulp.src(paths.source.images + '**/*.png')
    .pipe(size())
    // Select an optimization level between 0 and 7.
    .pipe(imagemin({optimizationLevel: 0}))
    .pipe(size())
    .pipe(gulp.dest(paths.build.images));
});

// ––––––––––––––––––––––––––––––––– IMAGES-JPG
gulp.task('images-jpg', function() {
  return gulp.src(paths.source.images + '**/*.jpg')
    .pipe(size())
    .pipe(imagemin({progressive: true}))
    .pipe(size())
    .pipe(gulp.dest(paths.build.images));
});

// ––––––––––––––––––––––––––––––––– IMAGES-ALL
gulp.task('images', ['images-png', 'images-jpg']);

// ––––––––––––––––––––––––––––––––– BROWSER-SYNC RELOAD
// I've got no idea why like this
gulp.task('bs-reload', function () {
  browserSync.reload();
});

// ––––––––––––––––––––––––––––––––– COMPILE
gulp.task('compile',['html', 'css', 'js', 'images']);

// ––––––––––––––––––––––––––––––––– WATCH-NO-NODE
gulp.task('watch-bs', ['clean', 'compile'], function () {
  gulp.watch('source/**/*.js'        , ['js']);
  gulp.watch('source/**/*.less'      , ['css']);
  gulp.watch('source/**/jade/**/*.*' , ['html']);
});

// ––––––––––––––––––––––––––––––––– WATCH
gulp.task('watch', function () {
  gulp.watch('source/**/*.js'          , ['js', browserSync.reload]);
  gulp.watch('source/**/*.less'        , ['css']);
  gulp.watch('./source/**/jade/**/*.*' , ['html', 'bs-reload']);
});

// ––––––––––––––––––––––––––––––––– BROWSER-SYNC
gulp.task('bs', ['watch-bs', 'browser-sync']);

// ––––––––––––––––––––––––––––––––– BROWSER-SYNC-NO-NODE [DEFAULT TAKS]
// not sure about order of dependencies
gulp.task('default', ['clean', 'compile','browser-sync-nodemon', 'watch']);


// ========================================
//   LIVE RELOAD
// https://github.com/sogko/gulp-recipes
// ========================================

// ––––––––––––––––––––––––––––––––– BROWSER-SYNC
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: "build"
    }
  });
});

// ––––––––––––––––––––––––––––––––– BROWSER-SYNC-NO-NODE
gulp.task('browser-sync-nodemon', ['nodemon'], function () {
  // more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync({
    proxy: 'http://localhost:3000',
    port: 4000,
    browser: ['google-chrome']
  });
});

// ––––––––––––––––––––––––––––––––– NODEMON
// we'd need a slight delay to reload browsers
// connected to browser-sync after restarting nodemon
var BROWSER_SYNC_RELOAD_DELAY = 500;
gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({

    // nodemon our expressjs server
    script: 'app.js',

    // watch core server file(s) that require server restart on change
    watch: ['app.js']
  })
    .on('start', function onStart() {
      // ensure start only got called once
      if (!called) { cb(); }
      called = true;
    })
    .on('restart', function onRestart() {
      // reload connected browsers after a slight delay
      setTimeout(function reload() {
        browserSync.reload({
          stream: false
        });
      }, BROWSER_SYNC_RELOAD_DELAY);
    });
});


